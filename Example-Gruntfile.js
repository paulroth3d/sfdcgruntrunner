/*global require, chalk, module*/
module.exports = function(grunt) {
	
	//-- CHANGE THIS LINE to the path of the sfdcgruntrunner folder
	//ex "~/Documents/sfdcgruntrunner";
	var sfdcgruntrunnerPath="../sfdcgruntrunner";
	
	//-- update this to /path/to/tasks folder
	grunt.loadTasks( sfdcgruntrunnerPath + '/tasks' );
	
	//-- update this to /path/to/grunt-sfdc-runner.js
	require( sfdcgruntrunnerPath + '/grunt-sfdc-runner.js' )(grunt);
	
	//-- override any configuration here as desired
	//-- see following for a list of jshint options - http://jshint.com/docs/options
	//-- NOTE: the files combined and ordering are within the package.xml file
	/** Example config setting.
	grunt.config.merge({
		"jshint":{
			"options":{
				node:true,
				newcap:false
			}
		},
		"uglify":{
			sfdc_uglify: {
				options: {
					mangle: false
				}
			}
		}
	});
	*/
};