#!/bin/bash

echo "What directory would you like to call Grunt from?"
echo "(we will copy the Example- templates to this directory"
echo "so running the command 'grunt sfdc_runner' will create static resources)"
read copyTargetDir
echo ""

echo "copying the example files:"
echo ""

echo "Copying Example-Gruntfile.js to ${copyTargetDir}/Gruntfile.js"
echo "(use this to overwrite properties from package.json)"
cp "Example-Gruntfile.js" "${copyTargetDir}/Gruntfile.js"
echo ""

myPath=`pwd`
export myPath
perl -pi -w -e 's/var sfdcgruntrunnerPath="[^"]*"/var sfdcgruntrunnerPath="$ENV{myPath}"/i' ${copyTargetDir}/Gruntfile.js

echo "copying default package.json"
echo "(use this to specify defaults to be overwritten in Gruntfile.js)"
cp "Example-package.json" "${copyTargetDir}/package.json"
echo ""

echo "copying .jshintignore"
echo "(update this to specify files to not be linted"
cp "Example-jshintignore" "${copyTargetDir}/.jshintignore"
echo ""

cd "${copyTargetDir}"
npm install

cd ${myPath}