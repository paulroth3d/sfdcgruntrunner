#Grunt Runner for Static Resources

### Overview:

The purpose of this project is to provide a simplified process for generating optimized Static Resources.

Currently, this is focused on JavaScript, but will expand to CSS (including SASS) and Image optimization.

**Whenever a static resource file is changed (or a new file is added to it), the tool will intelligently provide optimized versions of those resources.**

While the tools is meant to reduce the bar for those unfamiliar with Grunt, it provides a large amount of configuration to fit many needs.

### What is Grunt?

Grunt provides automation for repetitive tasks.  It can either execute those tasks on demand or by monitoring a set of files.

### How would I use this then?

Once it is setup, it knows and monitors which files are combined and minified into a production ready file.

Simply by running a single command of `grunt watch` - if any of those source files is simply saved, the production ready file is updated on the fly and is nearly immediately ready.

No additional commands are needed.

### Wislists

(please vote on the project page for areas on how to develop further)
* include support for SCSS
* include task to zip the js files to static resources
* include task to unzip / monitor static resources to explode them
* include task to upload to salesforce.

initial release was specifically focused on providing ease of use when working with js files.


## Installation Overview

* Clone the Project

* Run `setup.sh`

* Configure Gruntfile.js / package.js (if necessary)

* Run the project



#### System requirements

* NPM  - [http://blog.npmjs.org/post/85484771375/how-to-install-npm](http://blog.npmjs.org/post/85484771375/how-to-install-npm)

* grunt CLI - [http://gruntjs.com/installing-grunt](http://gruntjs.com/installing-grunt)<br /> or just `npm install grunt-cli grunt -g`


#### Cloning the Project

Then clone the project at [git@tig2.modelmetricssoftware.com:proth/sfdcgruntrunner.git](git@tig2.modelmetricssoftware.com:proth/sfdcgruntrunner.git)

NOTE: a single checkout can be used for many different projects.

	If you do not have credentials
	Please review the Services Wiki to obtain credentials for the Services GitLab instance.

For those not familiar with git, we recommend using [Git-Tower](git@tig2.modelmetricssoftware.com:proth/sfdcgruntrunner.git) or [Source-Tree](http://www.sourcetreeapp.com/), or reach out for help.


#### Running Setup.sh	

Within the terminal, navigate to the checkout and run:
`setup.sh`

This copies the necessary files to the directory for you to run `grunt` from.

It is recommended that the project follow a structure similar to the following <br /> (and should not have an package.json file)

	Ex:
	[projectFolder] (the folder passed)
		resources/
			StaticResource1_zip
				js/...
				css/...
				img/...
			StaticResource2_zip
				js/...
			etc.

(please note, this does a brief configuration and installs the npm modules for you.)

#### Installing npm modules

(Please note, that this is now included within setup.sh, but can always be re-run if more modules are needed for your project.)

The Setup.sh file simply copies the 'Example-*' templates into the project directory.


* navigate to that directory just provided <br /> and run `npm install` <br /> (this installs the submodules, for grunt to run)

Note, you can use the same sfdcgruntrunner checkout for multiple projects if desired.

* Run `grunt watch` to start <br /> (or `grunt watch --verbose` to verify the watched files)


#### Configuring

There are two main files:
* Gruntfile.js - Configures how grunt tasks perform
* Package.json - Configures files

##### Configuring Package.json

This defines the "which files and where" part.

There are three main configuration points:

* resourcesPath : this describes the folder that contains the 'exploded' static resource folders. (Files under concat are expected to be found at this location)

* resourceFolderPattern : regex describing how to tell which folders are exploded static resources.  (If in doubt, simply make the folder name match from resourcesPath)

* concat : this contains a list of static resources with the files that should be combined and how.


For example:

If there are two static resources:
	StaticResource1
		js
			view
				CalcView.js
			controller
				CalcController.js
			vendor
				JQuery.js
				Fabric.js
	StaticResource2
		Bootstrap.js
		JQuery.js
		...
				
We could then make minified files as follows:
	"concat": {
		"StaticResource1": {
			"js/min/app.js":["js/app.js","js/view/CalcView.js","js/view/CalcController.js"],
			"js/min/vendor.js":["js/vendor/**/*.js"]
		},
		"StaticResource2":{
			"vendor.js":["**/*.js"]
		}
	}

The files are added to the minified file in the order they are defined.

Also, note that .map files are also generated, so even though they are minified, they can still be inspected easily (human readible names, )			
See the Project.json deep dive section below for more information.

	Please note, a great amount of care was put to provide comments within the package.json file.  If a setting is confusing, please reach out via the 'help' section above.


##### Configuring Gruntfile.js

Changes to grunt commands (such as how to perform jshint/ uglify/ etc) can be done here.
Note that the modules used are all described in the package.json file.

(Its less likely this file needs to change than Package.json)

To find out more about each module, please see [http://gruntjs.com/plugins](http://gruntjs.com/plugins)



### Running the project

To run the project, simply run the following command in your project folder:
`grunt watch`

	Note, it is often recommended that on first run (or when changing configuration, use `grunt watch --verbose` as this will list the files being watched.)


Now, whenever any of the watched files are saved / changed, they are linted, combined, minified, mapped and uglified.




### Sample Package.json file

	{
	  "name": "MyProject",
	  "version": "1.0.0",
	  
	  "description": "[Description of your project]",
	  
	  "devDependencies": {
		"grunt-ant-sfdc": "^0.2.6",
		"grunt-contrib-clean": "^0.6.0",
		"grunt-contrib-compress": "^0.12.0",
		"grunt-contrib-concat": "^0.5.0",
		"grunt-contrib-copy": "^0.7.0",
		"grunt-contrib-jshint": "^0.10.0",
		"grunt-contrib-uglify": "^0.6.0",
		"grunt-contrib-watch": "^0.6.1"
	  },
	  
	  "sfdc_runner": {
		 "resourcesPath": "resources",
		 "resourceFolderPattern": "resources/([\\w_ ]+)/",
		 "concat": {			
			"pb_Foundation": {
				"js/foundation-min.js" : ["js/foundation/**/*.js"],
				"js/vendor-min.js" : ["js/vendor/**/*.js"]
			}
		 }
	  }
	}
	
### Sample Gruntfile.js

	/*global require, chalk, module*/
	module.exports = function(grunt) {
		
		//-- update this to /path/to/tasks
		grunt.loadTasks( '../sfdcgruntrunner/tasks' );
		
		//-- update this to /path/to/grunt-sfdc-runner.js
		require( "../sfdcgruntrunner/grunt-sfdc-runner.js" )(grunt);
		
		//-- override any configuration here as desired
		//-- see following for a list of jshint options - http://jshint.com/docs/options
		//-- NOTE: the files combined and ordering are within the package.xml file
		/** Example config setting.
		grunt.config.merge({
			"jshint":{
				"options":{
					node:true,
					newcap:false
				}
			},
			"uglify":{
				sfdc_uglify: {
					options: {
						mangle: false
					}
				}
			}
		});
		*/
	};

### Project.json deep dive

Configuration within your Package.json

	"sfdc_runner": {
		
Path to folder where the extracted static resources are found

EX: for sfdcantprojects, this could be : 'resources'",
EX: for mavensmate projects, this would likely be 'resource-bundles'",
		
		"resourcesPath": "resources",
		
regular expression used to determine the folder used as exploded static resource
this allows us to determine whether the file changed actually belongs to a static resource or not
the following assumes the folders found within resources are.

note: this would likely include the resourcesPath above

EX - sfdcantprojects: resources/([^/]+)/
EX - mavensmate     : resource-bundles/([^/]+)/
		
		"resourceFolderPattern": "resources/([\\w_ ]+)/",
		
List of resources with their minification file, and path
USE [RESOURCE] within the minification file path to use the name of the resource
EX: [RESOURCE]-min.js could provide resources/pb_Foundation_zip/pb_Foundation-zip.js
		
		"concat": {

<pre>EX:
pb_Foundation: {
	js/foundation-min.js : [js/foundation/**/*.js],
	js/vendor-min.js : [js/vendor/**/*.js]
}</pre>

		}
	}

